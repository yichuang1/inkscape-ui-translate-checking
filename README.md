How to check your translated UI language file(po/mo)?

1. Open your translated po file with poedit(https://packages.debian.org/stable/poedit), save the po file. When saving the po file, the mo file will be generated at the same time with the same file name.

2. Navigate to the directory where the inkscape appimage file is located, open a terminal and run `inkscape_appimage_name --appimage-extract` in the terminal.

3. After running the command, a new folder named squashfs-root was created. Navigate to `squashfs-root/usr/share/locale`, find your language, in the language folder, navigate into `LC_MESSAGES` folder, replace the mo file with your translated mo file created by poedit.

(Note: the file name must be the same as the original mo file name in the `LC_MESSAGES` folder)

4. After replacing the mo file, navigate to the folder of `squashfs-root/`,  and open a terminal and run `./AppRun` in the terminal.

5. Now the UI language should use your translated result. 
